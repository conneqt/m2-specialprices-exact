<?php

namespace Conneqt\ExactSpecialPrices\Model;

/**
 * @method \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice getResource()
 * @method \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\Collection getCollection()
 */
class SpecialPriceDataVolumePrice extends \Magento\Framework\Model\AbstractModel implements \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'exact_specialprices_specialpricedatavolumeprice';
    protected $_cacheTag = 'exact_specialprices_specialpricedatavolumeprice';
    protected $_eventPrefix = 'exact_specialprices_specialpricedatavolumeprice';

    protected function _construct()
    {
        $this->_init(\Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return mixed
     */
    public function getExactSpecialPriceId()
    {
        return $this->getData(self::FIELD_EXACT_SPECIALPRICE_ID);
    }

    /**
     * @var $specialPriceId int
     * @return \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setExactSpecialPriceId($specialPriceId)
    {
        $this->setData(self::FIELD_EXACT_SPECIALPRICE_ID, $specialPriceId);

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->getData(self::FIELD_QUANTITY);
    }

    /**
     * @param $quantity int
     * @return \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setQuantity($quantity)
    {
        $this->setData(self::FIELD_QUANTITY, $quantity);

        return $this;
    }

    /**
     * @return double
     */
    public function getValue()
    {
        return $this->getData(self::FIELD_VALUE);
    }

    /**
     * @param $value double
     * @return \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterface
     */
    public function setValue($value)
    {
        $this->setData(self::FIELD_VALUE, $value);

        return $this;
    }
}
