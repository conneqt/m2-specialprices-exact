<?php

namespace Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Conneqt\ExactSpecialPrices\Model\SpecialPriceDataVolumePrice::class, \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice::class);
    }

    /**
     * @param $specialPriceId
     * @return \Conneqt\ExactSpecialPrices\Model\SpecialPriceDataVolumePrice[]
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->addFieldToFilter('exact_specialprices_id', ['eq' => $specialPriceId])->getItems();
    }
}
