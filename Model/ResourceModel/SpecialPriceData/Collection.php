<?php

namespace Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(\Conneqt\ExactSpecialPrices\Model\SpecialPriceData::class, \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceData::class);
    }

    /**
     * @param $specialPriceId
     * @return \Conneqt\ExactSpecialPrices\Model\SpecialPriceData
     */
    public function findBySpecialPriceId($specialPriceId)
    {
        return $this->getItemByColumnValue(\Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataInterface::FIELD_SPECIALPRICE_ID, $specialPriceId);
    }
}
