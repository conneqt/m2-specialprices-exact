<?php

namespace Conneqt\ExactSpecialPrices\Model\ResourceModel;

class SpecialPriceDataVolumePrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('exact_pricevolumes', 'id');
    }
}
