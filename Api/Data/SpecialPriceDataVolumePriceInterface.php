<?php

namespace Conneqt\ExactSpecialPrices\Api\Data;

interface SpecialPriceDataVolumePriceInterface
{
    const FIELD_ID = 'id';
    const FIELD_EXACT_SPECIALPRICE_ID = 'exact_specialprices_id';
    const FIELD_QUANTITY = 'quantity';
    const FIELD_VALUE = 'value';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getExactSpecialPriceId();

    /**
     * @var $specialPriceId int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setExactSpecialPriceId($specialPriceId);

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param $quantity int
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setQuantity($quantity);

    /**
     * @return double
     */
    public function getValue();

    /**
     * @param $value double
     * @return \Conneqt\AvSpecialPrices\Api\Data\SpecialPriceDataInterface
     */
    public function setValue($value);
}
