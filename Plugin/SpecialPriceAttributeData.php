<?php

namespace Conneqt\ExactSpecialPrices\Plugin;

class SpecialPriceAttributeData
{
    /** @var \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataInterfaceFactory */
    protected $specialPriceDataInterfaceFactory;

    /** @var \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory */
    protected $specialPriceDataCollectionFactory;

    /** @var \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\CollectionFactory */
    protected $specialPriceDataVolumePriceCollectionFactory;

    /** @var \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterfaceFactory */
    protected $specialPriceDataVolumePriceInterfaceFactory;

    public function __construct(
        \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataInterfaceFactory $specialPriceDataInterfaceFactory,
        \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceData\CollectionFactory $specialPriceDataCollectionFactory,
        \Conneqt\ExactSpecialPrices\Model\ResourceModel\SpecialPriceDataVolumePrice\CollectionFactory $specialPriceDataVolumePriceCollectionFactory,
        \Conneqt\ExactSpecialPrices\Api\Data\SpecialPriceDataVolumePriceInterfaceFactory $specialPriceDataVolumePriceInterfaceFactory
    ) {
        $this->specialPriceDataInterfaceFactory = $specialPriceDataInterfaceFactory;
        $this->specialPriceDataCollectionFactory = $specialPriceDataCollectionFactory;
        $this->specialPriceDataVolumePriceCollectionFactory = $specialPriceDataVolumePriceCollectionFactory;
        $this->specialPriceDataVolumePriceInterfaceFactory = $specialPriceDataVolumePriceInterfaceFactory;
    }

    public function afterGetById(
        \Conneqt\SpecialPrices\Api\SpecialPriceRepositoryInterface $specialPriceRepository,
        \Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface $specialPrice
    ) {
        /** @var \Conneqt\SpecialPrices\Api\Data\SpecialPriceExtension $extensionAttributes */
        $extensionAttributes = $specialPrice->getExtensionAttributes();
        $specialPriceData = $this->specialPriceDataCollectionFactory->create()->findBySpecialPriceId($specialPrice->getId());
        if ($specialPriceData !== null) {
            $specialPriceVolumes = $this->specialPriceDataVolumePriceCollectionFactory->create()->findBySpecialPriceId($specialPriceData->getId());
            $specialPriceData->setPriceVolumes($specialPriceVolumes);

            $extensionAttributes->setCustom($specialPriceData);
            $specialPrice->setExtensionAttributes($extensionAttributes);
        }

        return $specialPrice;
    }

    public function afterBulkUpdate(\Conneqt\SpecialPrices\Api\SpecialPriceRepositoryInterface $specialPriceRepository, array $specialPrices)
    {
        foreach ($specialPrices as &$specialPrice) {
            $specialPrice = $this->handleExtensionAttributes($specialPrice);
        }

        return $specialPrices;
    }

    /**
     * @param \Conneqt\SpecialPrices\Api\SpecialPriceRepositoryInterface $specialPriceRepository
     * @param \Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface $specialPrice
     * @return \Conneqt\ExactSpecialPrices\Model\SpecialPriceData|\Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function afterSave(
        \Conneqt\SpecialPrices\Api\SpecialPriceRepositoryInterface $specialPriceRepository,
        \Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface $specialPrice
    ) {
        $specialPrice = $this->handleExtensionAttributes($specialPrice);

        return $specialPrice;
    }

    /**
     * @param $specialPrice \Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface
     * @return \Conneqt\ExactSpecialPrices\Model\SpecialPriceData|\Conneqt\SpecialPrices\Api\Data\SpecialPriceInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    private function handleExtensionAttributes($specialPrice)
    {
        $extensionAttributes = $specialPrice->getExtensionAttributes();
        if ($extensionAttributes->getCustom() !== null) {

            /** @var \Conneqt\ExactSpecialPrices\Model\SpecialPriceData $specialPrice */
            $specialPriceData = $this->specialPriceDataCollectionFactory->create()->findBySpecialPriceId($specialPrice->getId());
            if ($specialPriceData === null) {
                $specialPriceData = $this->specialPriceDataInterfaceFactory->create();
            }
            $specialPriceData->addData($extensionAttributes->getCustom()->getData());
            $specialPriceData->setSpecialPriceId($specialPrice->getId());
            $specialPriceData->getResource()->save($specialPriceData);

            if ($extensionAttributes->getCustom()->getPriceVolumes() !== null) {
                if ($specialPriceData->getId() !== null) {
                    $existingPriceVolumes = $this->specialPriceDataVolumePriceCollectionFactory->create()->findBySpecialPriceId($specialPriceData->getId());
                    foreach ($existingPriceVolumes as $priceVolume) {
                        $priceVolume->getResource()->delete($priceVolume);
                    }
                }

                $volumes = [];
                foreach ($extensionAttributes->getCustom()->getPriceVolumes() as $priceVolume) {
                    $volume = $this->specialPriceDataVolumePriceInterfaceFactory->create();
                    $volume->addData($priceVolume->getData());
                    $volume->setExactSpecialPriceId($specialPriceData->getId());
                    $volume->getResource()->save($volume);
                    $volumes[] = $volume;
                }

                $specialPriceData->setPriceVolumes($volumes);
            }

            $extensionAttributes->setCustom($specialPriceData);
            $specialPrice->setExtensionAttributes($extensionAttributes);
        }

        return $specialPrice;
    }
}
